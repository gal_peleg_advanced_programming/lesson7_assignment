﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarCards
{
    class Card
    {
        private int number;
        private char symbol;
        private Image image;

        public Card(int number, char symbol, Image image)
        {
            this.number = number;
            this.symbol = symbol;
            this.image = image;
        }
        /*
         * this function will create a card
         * using the code form the server
         * input: string of the code
         * output: none
         */
        public Card(string code)
        { 
            this.number = int.Parse(code.Substring(1,2));
            this.symbol = code[3];
            this.image = (Image)Properties.Resources.ResourceManager.GetObject("_" + number + symbol);
        }

        public int Number { get => number; set => number = value; }
        public char Symbol { get => symbol; set => symbol = value; }
        public Image Image { get => image; set => image = value; }

        /*
         * this function will generate code
         * for the server
         * input: none
         * output: code string of a card
         */
        public string GenerateCode()
        {
            return "1" + number.ToString("00") + symbol;
        }
        /*
         * this function will compare between two cards
         * input: other card
         * output: if the card is bigger than true else flase
         */
        public bool IsBigger(Card other)
        {
            return this.number > other.number;
        }
    }
}
