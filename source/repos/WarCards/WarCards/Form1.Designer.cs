﻿namespace WarCards
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.UserScore = new System.Windows.Forms.TextBox();
            this.RivalScore = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::WarCards.Properties.Resources.card_back_blue;
            this.pictureBox2.Location = new System.Drawing.Point(629, 79);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(99, 145);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(643, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // UserScore
            // 
            this.UserScore.BackColor = System.Drawing.Color.LimeGreen;
            this.UserScore.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UserScore.Location = new System.Drawing.Point(13, 13);
            this.UserScore.Name = "UserScore";
            this.UserScore.Size = new System.Drawing.Size(100, 13);
            this.UserScore.TabIndex = 3;
            // 
            // RivalScore
            // 
            this.RivalScore.BackColor = System.Drawing.Color.LimeGreen;
            this.RivalScore.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RivalScore.Location = new System.Drawing.Point(1258, 13);
            this.RivalScore.Name = "RivalScore";
            this.RivalScore.Size = new System.Drawing.Size(100, 13);
            this.RivalScore.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LimeGreen;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.RivalScore);
            this.Controls.Add(this.UserScore);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox UserScore;
        private System.Windows.Forms.TextBox RivalScore;
    }
}

