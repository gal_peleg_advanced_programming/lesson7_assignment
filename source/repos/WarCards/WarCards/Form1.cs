﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarCards
{
    public partial class Form1 : Form
    {
        private const int AmountOfCards = 10;
        private int userPoints;
        private int rivalPoints;
        private Card currCard;
        private Card rivalCard;
        private Card[] cards;
        private bool cardClicked;
        public Form1()
        {
            
            InitializeComponent();
            SetCards();
            //connect to the server
            byte[] buffer = new byte[4];
            TcpClient client = new TcpClient();
            try
            {
                IPEndPoint serverEndPoint = new
                IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                client.Connect(serverEndPoint);
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry cant connect to the server!");
                return;
            }
            NetworkStream clientStream = client.GetStream();
            byte[] bufferIn = new byte[4];
            int bytesRead = clientStream.Read(bufferIn, 0, 4);
            string input = new ASCIIEncoding().GetString(bufferIn);
            GenerateCards();// generate the cards after the server is connect
            Thread thread = new Thread(()=>Server(clientStream));//start the game from a new thread
            thread.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            userPoints = 0;
            rivalPoints = 0;
            UserScore.Text = "Your score: " + userPoints;
            RivalScore.Text = "Rival score: " + rivalPoints;
            cardClicked = false;
        }

        /*
         * this function creates 10 cards 
         * input: none
         * output: none
         */
        public void GenerateCards()
        {
            Point nextLocation = new Point(50, 500); //location to start from

            for(int i=0; i < AmountOfCards; i++)
            {
                PictureBox currPic = new PictureBox();
                currPic.Name = "card" + i;
                currPic.Location = nextLocation;
                currPic.Size = new Size(100, 142);
                currPic.Image = Properties.Resources.card_back_red;
                currPic.SizeMode = PictureBoxSizeMode.StretchImage;
                currPic.Click += delegate (object sender1, EventArgs e1)
                {
                    foreach(object picture in Controls)
                    {
                        if(picture is PictureBox)
                        {
                            if(((PictureBox)picture).Name != "pictureBox2")
                            {
                                ((PictureBox)picture).Image = Properties.Resources.card_back_red;
                            }
                        }
                    }
                    Random rnd = new Random();
                    int randomCard = rnd.Next(52);
                    currCard = cards[randomCard];
                    ((PictureBox)sender1).Image = currCard.Image;
                    cardClicked = true;

                };
                this.Controls.Add(currPic); // add to form
                nextLocation.X += 130; // move next location
                
            }
        }
        /*
         * this function will init a array with
         * 52 card objects
         * input: none 
         * output: none
         * */
        private void SetCards()
        {
            cards = new Card[52];
            char[] symbols = {'H', 'C', 'D', 'S'};
            for(int i=1,k=0;i<=13;i++)
            {
                for(int j=0;j<4;j++)
                {
                    Image image = (Image)Properties.Resources.ResourceManager.GetObject("_" + i + symbols[j]);
                    cards[k] = new Card(i, symbols[j], image);
                    k++;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Your score: " + userPoints + "\nRival score: " + rivalPoints);
            Environment.Exit(0);
        }
        /*
         * this function will be the function 
         * who handle the server 
         * as a thread
         * input: the client stream
         * output: none
         */
        private void Server(NetworkStream clientStream)
        {
            byte[] buffer = new byte[4];
            TcpClient client = new TcpClient();
            string input = "";
            byte[] bufferIn = new byte[4];
            while (input != "2000")
            {
                if (cardClicked) //if a card is clicking
                {
                    buffer = new ASCIIEncoding().GetBytes(currCard.GenerateCode());
                    clientStream.Write(buffer, 0, buffer.Length);
                    clientStream.Flush();
                    clientStream.Read(bufferIn, 0, 4);
                    input = new ASCIIEncoding().GetString(bufferIn);
                    rivalCard = new Card(input);
                    Invoke((MethodInvoker)delegate { pictureBox2.Image = rivalCard.Image; });
                    //check the scores
                    if(currCard.IsBigger(rivalCard))
                    {
                        userPoints++;
                        rivalPoints--;
                    }
                    else if(rivalCard.IsBigger(currCard))
                    {
                        rivalPoints++;
                        userPoints--;
                    }
                    //update score
                    Invoke((MethodInvoker)delegate {
                        UserScore.Text = "Your score: " + userPoints;
                        RivalScore.Text = "Rival score: " + rivalPoints;
                    });
                    cardClicked = false;
                }
                
            }
            //sending 2000 code to the server
            buffer = new ASCIIEncoding().GetBytes("2000");
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
            Invoke((MethodInvoker) delegate { MessageBox.Show("Your score: " + userPoints + "\nRival score: " + rivalPoints); });
            Invoke((MethodInvoker)delegate { Environment.Exit(0);});

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
